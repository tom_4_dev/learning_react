var webpack = require('webpack');  
module.exports = {  
  entry: [
    "./src/index.js"
  ],
  output: {
    path: __dirname + '/build',
    filename: "index.js"
  },
  devServer: {
    inline: true,
    port: 8080,
    historyApiFallback: true
  },
  module: {
    loaders: [
      { test: /\.jsx?$/, loaders: ['babel-loader'], exclude: /node_modules/ },
      { test: /\.css$/, loader: "style!css" }
    ]
  },
  plugins: [
    new webpack.NoErrorsPlugin()
  ]
};
