Guidelines taken from [Tutorial setting up SPA](http://jmfurlott.com/tutorial-setting-up-a-single-page-react-web-app-with-react-router-and-webpack/) and modified.

### Initializing

Initializing NPM and the Project
Starting with an empty directory for our project, we are gonna initialize a package.json to handle our package dependencies, and build commands. To do so, type the following and answer the corresponding questions:

```sh
$ npm init
```

Next, we will install the required packages and save them to the package.json. This allows for any body to quickly install all the required packages for your project by just typing npm install. For the first time, however, we will have to explicitly tell NPM what packages we need:

```sh
$ npm install --save-dev react webpack react-router react-hot-loader webpack-dev-server babel-loader babel-preset-es2015
```

With the create packages installed, we will now create the directories we will need for the project.

```sh
$ mkdir js css && touch index.html webpack.config.js
```

That's it!


### Webpack

Webpack is a module bundler that takes all your code and generates bundled static files. It is these files that will be ultimately included on our deployed web app.
Open the empty webpack.config.js, and paste in the following code.

```sh
var webpack = require('webpack');  
module.exports = {  
    entry: [
      'webpack/hot/only-dev-server',
      "./src/app.js"
    ],
    output: {
        path: __dirname + '/build',
        filename: "index.js"
    },
    devServer: {
      inline: true,
      port: 8080
    },  
    module: {
        loaders: [
          { test: /\.js?$/, loaders: ['babel-loader'], exclude: /node_modules/ },
          { test: /\.css$/, loader: "style!css" }
        ]
    },
    plugins: [
      new webpack.NoErrorsPlugin()
    ]
};
```

There are four main sections to this configuration file: entry, output, module, plugins.

The entry section tells webpack where the entry file is, and we have the development server for webpack and react-hot-loader. The output section is where the bundled files should go when we build our app. In this case, the files will be in /build/index.js. The module section is the most crucial part of webpack. This is where you add/remove loaders based on what you need webpack to bundle for you. The two main ones we are using are react-hot and babel. The line including react-hot and babel are for our development server to use es6 Javascript while hot loading. The babel-loader for when we build and generate static files that are to be es6-ified. Lastly, we generate our style.css as an example of how to include other types of static files. This is where you would put different loaders such as a sass-loader or one for image assets. The last section is plugins and include different type of webpack plugins. The NoErrorsPlugin is for hot loader to not automatically reload if there are errors in the code (this is useful for keeping state).

With the configuration set up, return to package.json and change your scripts section to reflect the following:

```sh
 "scripts": {
    "start": "webpack-dev-server --hot --progress --colors",
    "build": "webpack --progress --colors"
  }
```

### For Redux

To understand the initial flow of react redux I have gone through [Codementor Blog]https://www.codementor.io/mz026/getting-started-with-react-redux-an-intro-8r6kurcxf
and also took help from [Youtube.com]https://www.youtube.com/watch?v=R6BqfEbb-oU
