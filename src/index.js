import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { thunk } from 'redux-thunk';
import App from './App.jsx';
import helloReducer from './reducers/index.js'

const store = createStore(helloReducer);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('react')
);
