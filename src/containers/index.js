import React, { Component } from 'react';
import { connect } from 'react-redux';
import { HELLO_WORLD } from './../actions/index.js';
import Hello from './../Hello.js';

function mapStateToProps(state, ownProps) {
    return {
        message: state.helloWorld.message
    }
}

// which props do we want to inject, given the global store state?
function matchDispatchToProps(dispatch, ownProps) {
  return {
      onClick: () => {
          dispatch({ type: HELLO_WORLD })
      }
  };
}

const HelloWorld = connect(
    mapStateToProps, 
    matchDispatchToProps
)(Hello);

export default HelloWorld;
