import React from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';
import Login from './Login.jsx';
import About from './About.jsx';
import User from './User.jsx';
import HelloWorld from './containers/index.js'

class App extends React.Component {
   render() {
      return (
        <Router>
          <div>
            <ul>
              <li><Link to="/helloworld">Hello World With Redux</Link></li>
              <li><Link to="/login">Login</Link></li>
              <li><Link to="/add_user">Add User</Link></li>
              <li><Link to="/about">About</Link></li>
            </ul> 

            <Route path="/helloworld" component={HelloWorld} />
            <Route exact path="/login" component={Login} />
            <Route path="/add_user" component={User} />
            <Route path="/about" component={About} />
          </div> 
        </Router>
      );
   }
}

export default App;
